#!/bin/sh

case $BLOCK_BUTTON in
	3) notify-send "Biggest memory hoggs:
$(ps axch -o cmd:15,%mem --sort=-%mem | head -n 5)" ;;
esac

free -h | awk '/^Mem:/ {print $3}'
