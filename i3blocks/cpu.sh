#!/bin/sh

case $BLOCK_BUTTON in
	2)  ansiweather -l Vienna,AT -a false -s true -f 5 | sed 's/-/\n/g' | sed 's/Vienna forecast: //g '| while read OUTPUT; do notify-send "$OUTPUT"; done ;;
	3) notify-send "Biggest cpu hogs:
$(ps axch -o cmd:15,%cpu --sort=-%cpu | head -n 5)" ;;
esac

sensors | awk '/^Package/ {print $4}' | cut -d "+" -f 2
