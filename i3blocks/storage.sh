#!/bin/sh

case $BLOCK_BUTTON in
	2) notify-send "$(lsblk -l -o NAME,FSSIZE,FSUSE%,FSTYPE,MOUNTPOINT)" ;;
	3) notify-send " $(lsblk -o RM,NAME,MODEL,LABEL,MOUNTPOINT | awk '$1 == "1" {print $2 "    " $3 "    "  $4 "    " $5}')"
esac

df -h | awk '/sdb3/ {print $3}'
