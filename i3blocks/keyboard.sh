#!/bin/sh

case $BLOCK_BUTTON in
    1) setxkbmap -layout de ;;
    2) setxkbmap -layout hr ;;
    3) layout-switcher.sh ;;
esac
setxkbmap -query | grep layout | cut -b 13-15

